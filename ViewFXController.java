
package newlotto;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.ResourceBundle;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;


public class ViewFXController implements Initializable {
    //class vars.
    //max and min numbers for random nunber generation (RNG)
    private final int MAX = 99;
    private final int MIN = 1;
    //texts for winning/losing
    private final String w0 = "Vesztettél";
    private final String w1 = "1-esed van a lottón";
    private final String w2 = "2-esed van a lottón";
    private final String w3 = "3-asod van a lottón";
    private final String w4 = "4-esed van a lottón";
    private final String w5 = "5-ösöd van a lottón";
    
    //number selecters
    private int genNum1;
    private int genNum2;
    private int genNum3;
    private int genNum4;
    private int genNum5;
    
    private int selNum1;
    private int selNum2;
    private int selNum3;
    private int selNum4;
    private int selNum5;
    
    int result;
    //fxml elements
    @FXML
    private Pane basePane;
    @FXML
    private Pane alertPane;
    @FXML
    private TextField input1;
    @FXML
    private TextField input2;
    @FXML
    private TextField input3;
    @FXML
    private TextField input4;
    @FXML
    private TextField input5;
    @FXML
    private Label label1;
    @FXML
    private Label label2;
    @FXML
    private Label label3;
    @FXML
    private Label label4;
    @FXML
    private Label label5;
    @FXML
    private Label alertText;
    @FXML
    private Label resultLabel;
    @FXML
    private Button draButton;
    @FXML
    private Button cButton;
    @FXML
    private Button alertButton;
    @FXML
    private Button exitButton;
    
    @FXML
    private void exit(ActionEvent event){
        System.exit(0);
    }
    
    @FXML
    private void clearAll(ActionEvent event){
        genNum1 = 0;
        genNum2 = 0;
        genNum3 = 0;
        genNum4 = 0;
        genNum5 = 0;
        
        selNum1 = 0;
        selNum2 = 0;
        selNum3 = 0;
        selNum4 = 0;
        selNum5 = 0;
                
        result = 0;
        
        input1.setText(null);
        input2.setText(null);
        input3.setText(null);
        input4.setText(null);
        input5.setText(null);
        
        label1.setText(null);
        label2.setText(null);
        label3.setText(null);
        label4.setText(null);
        label5.setText(null);
       
        resultLabel.setText(null);
    }
    
    @FXML
    private void handleAlertButton(ActionEvent event) {
        basePane.setDisable(false);
        basePane.setOpacity(1);
        alertPane.setVisible(false);
        alertText.setText("");
    }
    @FXML
    private void handleButtonAction(ActionEvent event) {
        //random number generation
        genNum1 = 0;
        genNum2 = 0;
        genNum3 = 0;
        genNum4 = 0;
        genNum5 = 0;
        genNum1 = getRandomNumber();
        genNum2 = getRandomNumber();
        genNum3 = getRandomNumber();
        genNum4 = getRandomNumber();
        genNum5 = getRandomNumber();
        Calculation();
    }
    
    private void Calculation(){
        //only numbers can be given
        try {
            selNum1 = Integer.parseInt(input1.getText());
            selNum2 = Integer.parseInt(input2.getText());
            selNum3 = Integer.parseInt(input3.getText());
            selNum4 = Integer.parseInt(input4.getText());
            selNum5 = Integer.parseInt(input5.getText());
        } catch (Exception e) {
            alert("A számok nem jók!");
            return;
        }
        
        //are they unique?
        HashSet<Integer> selectedNumbers = new HashSet<>();
        selectedNumbers.add(selNum1);
        selectedNumbers.add(selNum2);
        selectedNumbers.add(selNum3);
        selectedNumbers.add(selNum4);
        selectedNumbers.add(selNum5);
        if (selectedNumbers.size() > 5 ) {
         alert("A számoknak különbözőknek kell lennie!");
         return;
        }
         ArrayList<Integer> userNumbers = new ArrayList<>(selectedNumbers);
         //are they between 1 and 99?
         for(Integer number : userNumbers){
            if (number < MIN || number > MAX){
                alert("Minden számnak 1 és 99 között kell lennie!");
                return;
            }
        }
        label1.setText("" + genNum1);
        label2.setText("" + genNum2);
        label3.setText("" + genNum3);
        label4.setText("" + genNum4);
        label5.setText("" + genNum5);
        
        resultCheck(userNumbers);
               
        return;
    }
     private void alert(String text){
         basePane.setDisable(true);
         basePane.setOpacity(0.3);
         alertPane.setVisible(true);
         alertText.setText(text);
    }
    
    private void resultCheck(ArrayList<Integer> userNumbers){
       result = 0; 
        for(int i=0;i<userNumbers.size();i++){
            if(userNumbers.get(i) == genNum1 || userNumbers.get(i) == genNum2 || userNumbers.get(i) == genNum3 || userNumbers.get(i) == genNum4 || userNumbers.get(i) == genNum5)
                result++;
        }
        
        switch(result){
            case 0 : resultLabel.setText(w0);
                    break;
            case 1 : resultLabel.setText(w1);
                    break;
            case 2 : resultLabel.setText(w2);
                    break;
            case 3 : resultLabel.setText(w3);
                    break;
            case 4 : resultLabel.setText(w4);
                    break;
            case 5 : resultLabel.setText(w5);
                    break;
        }
    }
    
    
    
    
    private int getRandomNumber(){
      int random = (int) (Math.random() * MAX) + MIN; 
      
      if (random == genNum1 || random == genNum2 || random == genNum3 || random == genNum4 || random == genNum5){
          return getRandomNumber();
      }
      
      return random;      
    }
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // 
    }    
    }
 
    
    

